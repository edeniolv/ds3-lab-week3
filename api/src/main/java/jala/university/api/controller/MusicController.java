package jala.university.api.controller;

import jala.university.api.model.entity.Music;
import jala.university.api.model.service.MusicService;
import lombok.extern.java.Log;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import java.util.UUID;
import java.util.logging.Level;


@Log
@RestController
@RequestMapping("api/music")
public class MusicController {
    private final MusicService service;

    @Autowired
    public MusicController(MusicService service) {
        this.service = service;
    }

    @PostMapping
    public ResponseEntity<HttpStatus> createMusic(@RequestBody Music music) {
        if (service.create(music) == null) {
            log.log(Level.WARNING, "Music not created");
            return new ResponseEntity<>(HttpStatus.INTERNAL_SERVER_ERROR);
        }

        log.log(Level.INFO, "Music created successfully");
        return new ResponseEntity<>(HttpStatus.OK);
    }

    @GetMapping("/all")
    public ResponseEntity<Iterable<Music>> readAllMusic() {
        Iterable<Music> musics = service.readAll();

        if (musics == null) {
            log.log(Level.FINER, "No music found");
            return new ResponseEntity<>(HttpStatus.NOT_FOUND);
        }

        log.log(Level.INFO, "Sent all music");
        return new ResponseEntity<>(musics, HttpStatus.OK);
    }

    @GetMapping("/title/{title}")
    public ResponseEntity<Music> readMusicByTitle(@PathVariable String title) {
        Music music = service.realByTitle(title);

        if (music == null) {
            log.log(Level.WARNING, "Failed to find music by title");
            return new ResponseEntity<>(HttpStatus.NOT_FOUND);
        }

        log.log(Level.INFO, "Sent music by title");
        return new ResponseEntity<>(music, HttpStatus.OK);
    }

    @GetMapping("/artist/{name}")
    public ResponseEntity<Iterable<Music>> readMusicByArtist(@PathVariable String name) {
        Iterable<Music> music = service.realByArtist(name);

        if (music == null) {
            log.log(Level.WARNING, "Failed to find music by artist name");
            return new ResponseEntity<>(HttpStatus.NOT_FOUND);
        }

        log.log(Level.INFO, "Sent music by artist name");
        return new ResponseEntity<>(music, HttpStatus.OK);
    }

    @GetMapping("/gender/{name}")
    public ResponseEntity<Iterable<Music>> readMusicByGender(@PathVariable String name) {
        Iterable<Music> music = service.realByGender(name);

        if (music == null) {
            log.log(Level.WARNING, "Failed to find music by gender name");
            return new ResponseEntity<>(HttpStatus.NOT_FOUND);
        }

        log.log(Level.INFO, "Sent music by gender name");
        return new ResponseEntity<>(music, HttpStatus.OK);
    }

    @PostMapping("/{id}")
    public ResponseEntity<HttpStatus> updateMusic(@PathVariable UUID id, @RequestBody Music music) {
        if (service.update(id, music) == null) {
            log.log(Level.WARNING, "Failed to update music");
            return new ResponseEntity<>(HttpStatus.INTERNAL_SERVER_ERROR);
        }

        log.log(Level.INFO, "Music updated successfully");
        return new ResponseEntity<>(HttpStatus.CREATED);
    }

    @DeleteMapping("/remove/{id}")
    public ResponseEntity<HttpStatus> deleteMusic(@PathVariable UUID id) {
        if (service.delete(id) == 0) {
            return new ResponseEntity<>(HttpStatus.INTERNAL_SERVER_ERROR);
        }

        log.log(Level.INFO, "Music deleted successfully");
        return new ResponseEntity<>(HttpStatus.OK);
    }
}
