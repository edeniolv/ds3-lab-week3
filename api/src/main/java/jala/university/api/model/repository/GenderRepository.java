package jala.university.api.model.repository;

import jala.university.api.model.entity.Gender;
import org.springframework.data.repository.CrudRepository;

import java.util.UUID;

public interface GenderRepository extends CrudRepository<Gender, UUID> {
    Gender findByName(String name);
}
