package jala.university.api.model.repository;

import jala.university.api.model.entity.Artist;
import org.springframework.data.repository.CrudRepository;

import java.util.UUID;

public interface ArtistRepository extends CrudRepository<Artist, UUID> {
    Artist findByName(String name);
}
