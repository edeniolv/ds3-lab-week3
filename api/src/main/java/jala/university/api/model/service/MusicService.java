package jala.university.api.model.service;

import jala.university.api.model.entity.Artist;
import jala.university.api.model.entity.Gender;
import jala.university.api.model.entity.Music;
import jala.university.api.model.repository.MusicRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.UUID;

@Service
public class MusicService {
    private final MusicRepository repository;
    private final ArtistService artistService;
    private final GenderService genderService;

    @Autowired
    public MusicService(
        MusicRepository repository,
        ArtistService artistService,
        GenderService genderService
    ) {
        this.repository = repository;
        this.artistService = artistService;
        this.genderService = genderService;
    }

    public Music create(Music newMusic) {
        Music oldMusic = repository.findByTitle(newMusic.getTitle());

        Artist artist = artistService.create(newMusic.getArtist());

        if (artist == null) {
            return null;
        }

        Gender gender = genderService.create(newMusic.getGender());

        if (gender == null) {
            return null;
        }

        if (oldMusic != null && newMusic.getTitle().equals(oldMusic.getTitle())) {
            return null;
        }

        newMusic.setArtist(artist);
        newMusic.setGender(gender);

        return repository.save(newMusic);
    }

    public Iterable<Music> readAll() {
        return repository.findAll();
    }

    public Music realByTitle(String title) {
        if (title.isEmpty()) {
            return null;
        }

        return repository.findByTitle(title);
    }

    public Iterable<Music> realByArtist(String name) {
        if (name.isEmpty()) {
            return null;
        }

        Artist artist = artistService.readByName(name);

        if (artist == null) {
            return null;
        }

        return repository.findByArtist(artist);
    }

    public Iterable<Music> realByGender(String name) {
        if (name.isEmpty()) {
            return null;
        }

        Gender artist = genderService.readByName(name);

        if (artist == null) {
            return null;
        }

        return repository.findByGender(artist);
    }

    public Music update(UUID id, Music newMusic) {
        Music oldMusic = repository.findByTitle(newMusic.getTitle());

        if (!id.equals(oldMusic.getId())) {
            return null;
        }

        Artist artist = artistService.update(newMusic.getArtist());

        if (artist == null) {
            return null;
        }

        Gender gender = genderService.update(newMusic.getGender());

        if (gender == null) {
            return null;
        }

        oldMusic.setArtist(artist);
        oldMusic.setGender(gender);

        return repository.save(oldMusic);
    }

    public Integer delete(UUID id) {
        return repository.deleteByIdEquals(id);
    }
}
