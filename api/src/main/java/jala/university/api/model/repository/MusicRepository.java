package jala.university.api.model.repository;

import jakarta.transaction.Transactional;
import jala.university.api.model.entity.Artist;
import jala.university.api.model.entity.Gender;
import jala.university.api.model.entity.Music;
import org.springframework.data.repository.CrudRepository;

import java.util.UUID;

public interface MusicRepository extends CrudRepository<Music, UUID> {
    Music findByTitle(String title);
    Iterable<Music> findByArtist(Artist artist);
    Iterable<Music> findByGender(Gender gender);
    @Transactional
    Integer deleteByIdEquals(UUID id);
}
