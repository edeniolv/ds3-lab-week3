package jala.university.api.model.service;

import jala.university.api.model.entity.Artist;
import jala.university.api.model.repository.ArtistRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

@Service
public class ArtistService {
    private final ArtistRepository repository;

    @Autowired
    public ArtistService(ArtistRepository repository) {
        this.repository = repository;
    }

    public Artist create(Artist newArtist) {
        return validate(newArtist);
    }

    public Artist readByName(String name) {
        if (name.isEmpty()) {
            return null;
        }

        return repository.findByName(name);
    }

    public Artist update(Artist newArtist) {
        return validate(newArtist);
    }

    private Artist validate(Artist newArtist) {
        if (newArtist == null) {
            return null;
        }

        Artist oldGender = repository.findByName(newArtist.getName());

        if (oldGender != null && newArtist.getName().equals(oldGender.getName())) {
            newArtist = oldGender;
        }

        return repository.save(newArtist);
    }
}
