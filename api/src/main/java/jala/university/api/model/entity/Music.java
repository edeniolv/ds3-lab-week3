package jala.university.api.model.entity;

import jakarta.persistence.*;
import lombok.Data;

import java.util.UUID;

@Data
@Entity
@Table(name = "music")
public class Music {
    @Id
    @GeneratedValue(strategy = GenerationType.UUID)
    @Column(name = "id", nullable = false)
    private UUID id;

    @Column(name = "title", nullable = false)
    private String title;

    @Column(name = "duration", nullable = false)
    private String duration;

    @OneToOne
    @JoinColumn(name = "artist_fk")
    private Artist artist;

    @OneToOne
    @JoinColumn(name = "gender_fk")
    private Gender gender;
}
