package jala.university.api.model.service;

import jala.university.api.model.entity.Gender;
import jala.university.api.model.repository.GenderRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

@Service
public class GenderService {
    private final GenderRepository repository;

    @Autowired
    public GenderService(GenderRepository repository) {
        this.repository = repository;
    }

    public Gender create(Gender newGender) {
        return validate(newGender);
    }

    public Gender readByName(String name) {
        if (name.isEmpty()) {
            return null;
        }

        return repository.findByName(name);
    }

    public Gender update(Gender newGender) {
        return validate(newGender);
    }

    private Gender validate(Gender newGender) {
        if (newGender == null) {
            return null;
        }

        Gender oldGender = repository.findByName(newGender.getName());

        if (oldGender != null && newGender.getName().equals(oldGender.getName())) {
            newGender = oldGender;
        }

        return repository.save(newGender);
    }
}
